if "%~1"=="" goto err
cd /d "%~dp0"|| goto err
set dst=%~1
if "%dst:~1%"==":" goto ok
if not "%dst:~1%"==":\" goto err
:ok
set dst=%~d1
if not "%dst:~1%"==":" goto err

copy grldr %dst%|| goto err
attrib +R +S +H %dst%\grldr|| goto err
copy plpbt.bin %dst%|| goto err
attrib +R +S +H %dst%\plpbt.bin|| goto err
copy menu.lst %dst%|| goto err
attrib +R +S +H %dst%\menu.lst|| goto err
copy default %dst%|| goto err
attrib +R +S +H %dst%\default|| goto err
copy wininst.tag %dst%|| goto err
attrib +R +S +H %dst%\wininst.tag|| goto err
copy winsetup.lst %dst%|| goto err
attrib +R +S +H %dst%\winsetup.lst|| goto err
copy windefault %dst%|| goto err
attrib +R +S +H %dst%\windefault|| goto err
copy BOOTSECT.DOS %dst%|| goto err
attrib +R +S +H %dst%\BOOTSECT.DOS|| goto err

xcopy /I /S /E BOOTWS %dst%\BOOTWS|| goto err
attrib +S +H %dst%\BOOTWS|| goto err

xcopy /I /S /E WINSETUP %dst%\WINSETUP|| goto err

pause
exit /b 0

:err
pause
exit /b 1
