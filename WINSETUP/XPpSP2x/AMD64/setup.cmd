rem Special thanks to cdob 
rem http://www.msfn.org/board/install-2000-xp-2003-ram-loaded-small-iso-t139737-page-20.html

@echo off
setlocal EnableExtensions EnableDelayedExpansion

if not exist reg.exe (
echo reg.exe not found 
echo reg.exe not found >> %SystemRoot%\winsetup.log
)
if not exist findstr.exe (
echo findstr.exe not found 
echo findstr.exe not found >> %SystemRoot%\winsetup.log
)

if not exist mountvol.exe (
echo findstr.exe not found 
echo findstr.exe not found >> %SystemRoot%\winsetup.log
) else (
echo mountvol.exe output: >> %SystemRoot%\winsetup.log
mountvol.exe >> %SystemRoot%\winsetup.log
)

rem XPpSP2x string is dynamically changed by WinSetupFromUSB when prepariung source
set TAGFILE=\WINSETUP\XPpSP2x

FOR %%h IN (U V W X C D E F G H I J K L M N O P Q R S T Y) DO (
IF EXIST "%%h:%TAGFILE%" (
SET CDDRIVE=%%h:
goto :CDDRIVE_found
)
)

echo no %TAGFILE% drive found
echo no %TAGFILE% drive found >> %SystemRoot%\winsetup.log
pause
pause


:CDDRIVE_found ===============================================
set srcpath=\??\%CDDRIVE%%TAGFILE%\

echo CDDRIVE %CDDRIVE% - srcpath %srcpath%
echo CDDRIVE %CDDRIVE% - srcpath %srcpath% >> %SystemRoot%\winsetup.log

if exist $winnt$.new del /f $winnt$.new
FOR /F "tokens=1* delims== " %%a in ($winnt$.inf) DO (
set line=%%a = %%b
if %%b.==. set line=%%a
rem requires a drive letter still
rem IF /I sourcepath.==%%a. set line=sourcepath = "\\?\GLOBALROOT\Device\Harddisk1\Partition1\WIN_SETUP\XP_X64"
IF /I sourcepath.==%%a. set line=sourcepath = "%srcpath%"
IF /I dospath.==%%a. set line=;
echo !line!>>$winnt$.new
)
if exist $winnt$.new (
ren $winnt$.inf $winnt$.0
if exist $winnt$.inf del /f $winnt$.inf
ren $winnt$.new $winnt$.inf
)

findstr.exe "sourcepath" $winnt$.inf
findstr.exe "sourcepath" $winnt$.inf >> %SystemRoot%\winsetup.log


REM +=======================================================+
REM | Finally start the installation |
REM |-------------------------------------------------------|
%SystemDrive%
cd %SystemRoot%\system32

echo deleting setupol.exe >> %SystemRoot%\winsetup.log
if exist setupol.exe del /f setupol.exe >> %SystemRoot%\winsetup.log
echo renaming setup.exe to setupol.exe >> %SystemRoot%\winsetup.log
if exist setup.exe ren setup.exe setupol.exe
echo renaming setupWST.exe to setup.exe >> %SystemRoot%\winsetup.log
if exist setupWST.exe ren setupWST.exe setup.exe
echo finished renaming setup files >> %SystemRoot%\winsetup.log


echo start /min %CDDRIVE%%TAGFILE%\AMD64\HelpAss.bat >> %SystemRoot%\winsetup.log
start /min %CDDRIVE%%TAGFILE%\AMD64\HelpAss.bat

rem often setup.exe -newsetup
rem workaround for 2000 which does not include reg.exe by default
if exist reg.exe (
goto :startXP
) else (
goto :start2000
)

echo exiting: %date% %time% >> %SystemRoot%\winsetup.log
EXIT
goto :eof ==============end CDDRIVE_found=============================

:startXP
FOR /F "tokens=1-2*" %%a IN ('reg.exe query "HKLM\SYSTEM\Setup" /v "CmdLine"') DO set CmdLine=%%c
IF "%CmdLine:~-2%"=="\0" set CmdLine=%CmdLine:~0,-2%
echo run setup CmdLine: %CmdLine% - %date% %time%
echo run setup CmdLine: %CmdLine% - %date% %time% >> %SystemRoot%\winsetup.log
start /WAIT %CmdLine%
goto :eof


:start2000
echo reg.exe not found, launching "start /WAIT setup.exe -newsetup"
echo reg.exe not found, launching "start /WAIT setup.exe -newsetup" >> %SystemRoot%\winsetup.log
start /WAIT setup.exe -newsetup
goto :eof

