@echo off
title Don't close this window
echo This script will create "HelpAssistant" user for being aligned with 32-bit XP
echo waiting for network services...

:loop
	timeout /t 1 >nul
	net user HelpAssistant /add /active:no /passwordchg:no /passwordreq:no
if not errorlevel 0 goto loop
if errorlevel 1 goto loop

echo DONE!
timeout /t 10 >nul
exit
